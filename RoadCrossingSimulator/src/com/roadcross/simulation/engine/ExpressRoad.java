package com.roadcross.simulation.engine;

import java.awt.*;
import java.util.LinkedList;
import java.util.concurrent.Semaphore;

public class ExpressRoad {

    public int height, width;
    public static final int maxVehicles = 4;
    private Semaphore vehicleProtect = new Semaphore(1);
    private boolean[] laneOccupied = new boolean[maxVehicles];

    // add/remove with
    // semaphore
    private LinkedList<RoundShape> mVehicleList = new LinkedList<RoundShape>();

    public void newVehicle(int n) {
        try {
            vehicleProtect.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < n; i++) {
            if (mVehicleList.size() >= maxVehicles)
                break;
            RoundShape vehicle = new RoundShape(0, 0,RoundShape.globalRoundShapeRadius);
            vehicle.x = Math.random() * (width - 2 * vehicle.rad) + vehicle.rad;
            vehicle.y = getVehicleYPosition();

            vehicle.vx = 5 * (Math.random());
            vehicle.vy = 0;
            mVehicleList.add(vehicle);
        }
        vehicleProtect.release();
    }

    public LinkedList<RoundShape> getVehicleList() {
        return mVehicleList;
    }


    public void update(int w, int h) {
        this.width = w;
        this.height = h;
        for (RoundShape p : mVehicleList) {
            p.updatePosition();
            p.checkCollision(0, w);
        }
    }

    public void reset() {
        try {
            vehicleProtect.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mVehicleList.clear();
        vehicleProtect.release();
    }

    public double getVehicleYPosition() {
        int ypos = RoundShape.globalRoundShapeRadius*3;
        int numOfLans = laneOccupied.length * 2;
        if (this.height == 0) {
            return ypos;
        }
        int lanSize = height / maxVehicles;

        while (numOfLans > 0) {
            ypos = (int) (Math.random() * height);
            int vehicleLane = ypos / lanSize;
            if (vehicleLane <= laneOccupied.length - 1 && !laneOccupied[vehicleLane]) {
                laneOccupied[vehicleLane] = true;
                ypos = (vehicleLane * lanSize) + (lanSize) / 2;
                break;
            } else {
                numOfLans--;
            }
        }
        return ypos;
    }

    public void draw(Graphics g) {

        int lanSize = height/maxVehicles;
        int currentLanYPosition =lanSize;
        g.setColor(Color.WHITE);
        for (int i = 0; i < maxVehicles-1 && currentLanYPosition<height; i++,currentLanYPosition+=lanSize) {
            int stripeXPosition = 50;
            while (stripeXPosition<width-100){
                g.drawLine(stripeXPosition,currentLanYPosition,stripeXPosition+100,currentLanYPosition);
                stripeXPosition+=200;
            }

        }


        Color colorA = Color.YELLOW ;
        Color colorB = Color.WHITE ;
        int padestrianWidth = width/(maxVehicles*2);
        int currentPedeStrianYPos = 0;
        boolean flag=false;
        while (currentPedeStrianYPos<width){
            if(flag){
                g.setColor(colorA);
            }else{
                g.setColor(colorB);
            }
            flag=!flag;
            g.fill3DRect(currentPedeStrianYPos, 0, currentPedeStrianYPos + padestrianWidth,20, true);
            g.fill3DRect(currentPedeStrianYPos, height-20, currentPedeStrianYPos+ padestrianWidth, height, true);
            currentPedeStrianYPos+=padestrianWidth;
        }

        g.setColor(Color.GRAY);
        for (RoundShape vehicle : mVehicleList) {
            g.fillRect((int) (vehicle.x - vehicle.rad), (int) (vehicle.y - vehicle.rad), (int) (2 * vehicle.rad + 1), (int) (2 * vehicle.rad + 1));
        }
    }

}
