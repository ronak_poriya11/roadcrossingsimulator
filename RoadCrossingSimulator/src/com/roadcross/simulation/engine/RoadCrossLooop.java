package com.roadcross.simulation.engine;

import com.roadcross.simulation.neuralNetwork.DNA;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created by ronakporiya on 25/4/17.
 */
public class RoadCrossLooop extends JComponent {

    public static final int numChild = 10;
    public LinkedList<Child> childrenList = new LinkedList<>();
    public double childMaxFitness = 0;

    public static double mMutationRate = .03;
    public double mGenerationNumber = 0;

    public ExpressRoad expressRoad = new ExpressRoad();
    public static final long FRAMERATE = 8;
    private long mLastFrameTimeInMilliSeconds;

    public RoadCrossLooop() {
        mLastFrameTimeInMilliSeconds = System.currentTimeMillis()+100;
        Thread mFrameUpdationThread = new Thread(new FrameRunnable());
        mFrameUpdationThread.start();
    }

    class FrameRunnable implements Runnable {
        int childDeadCounter = 0;

        @Override
        public void run() {
            while (true) {
                long timeDiff = System.currentTimeMillis() - mLastFrameTimeInMilliSeconds;
                if (timeDiff > FRAMERATE) {
                    expressRoad.update(getWidth(), getHeight());
                    synchronized (childrenList) {
                        if (childrenList.isEmpty()) {
                            for (int i = 0; i < numChild; i++) {
                                childrenList.add(new Child(null, expressRoad));
                            }
                            expressRoad.reset();
                            expressRoad.newVehicle(ExpressRoad.maxVehicles);
                        }

                        childDeadCounter = 0;
                        for (Child child : childrenList) {
                            if (!child.update(expressRoad)) {
                                childDeadCounter++;
                            }
                            if (child.getFitnessInfo() > childMaxFitness)
                                childMaxFitness = child.getFitnessInfo();
                        }

                        for (int i = 0; i < childDeadCounter; i++) {
                            addNewChild();
                            mGenerationNumber += 1 / (double) numChild;
                        }

                        Iterator<Child> childIterator = childrenList.iterator();
                        while (childIterator.hasNext()) {
                            Child child = childIterator.next();
                            if (child.deathAlpha <= 0) {
                                childIterator.remove();
                            }
                        }
                        repaint();
                        mLastFrameTimeInMilliSeconds += FRAMERATE;
                    }
                }
            }
        }
    }


    /**
     * Using genetic algo : Select any 2 random child from matting pool and generate new child from selected child's DNA
     */
    public void addNewChild() {
        mMutationRate = 10 / childMaxFitness;
        ArrayList<Child> matingpool = makeMatingpool();
        DNA childOne = matingpool.get((int) (Math.random() * matingpool.size())).dna;
        DNA childTwo = matingpool.get((int) (Math.random() * matingpool.size())).dna;
        childrenList.add(new Child(childOne.crossoverBytewise(childTwo, mMutationRate), expressRoad));
    }


    public ArrayList<Child> makeMatingpool() {
        ArrayList<Child> matingpool = new ArrayList<Child>();
        // get maximum fitness:
        double maxscore = 0;
        for (Child child : childrenList) {
            if (child.getFitnessInfo() > maxscore) {
                maxscore = child.getFitnessInfo();
            }
        }
        for (Child child : childrenList) {
            int amount = (int) (child.getFitnessInfo() * 100 / maxscore);
            for (int i = 0; i < amount; i++) {
                matingpool.add(child);
            }
        }
        return matingpool;
    }


    /**
     * Show graphics
     */
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        // Background:
        g.setColor(Color.black);
        g.fillRect(0, 0, getWidth(), getHeight());

       /* if(!childrenList.isEmpty()){
             childrenList.getFirst().neuralNet.display(g, 0, expressRoad.width, expressRoad.height);
        }*/

        synchronized (childrenList) {
            for (Child child : childrenList)
                child.draw(g);
            expressRoad.draw(g);
        }
    }

}



