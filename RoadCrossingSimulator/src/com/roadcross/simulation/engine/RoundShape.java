package com.roadcross.simulation.engine;

public class RoundShape {

	public double x;
	public double y;
	public double vx = 0;
	public double vy = 0;
	public double rad;
	public static final int globalRoundShapeRadius = 20;


	public RoundShape(double x, double y, double rad) {
		this.x = x;
		this.y = y;
		this.rad = rad;
	}
	

	public void checkCollision(double xmin, double xmax) {
		if (x - rad < 0) {
			x = xmin + rad;
			vx = -vx ;
		}
		if (x + rad > xmax) {
			x = xmax - rad;
			vx = -vx ;
		}
	}


	public void updatePosition() {
		x += vx;
		y += vy;
	}

	public boolean isColliding(RoundShape o, double thresholdDistance) {
		double d = Math.sqrt((this.x - o.x) * (this.x - o.x) + (this.y - o.y) * (this.y - o.y));
		double s = this.rad + o.rad;
		return d < s + thresholdDistance;
	}

	public double getSpeedInfo() {
		return Math.sqrt(vx * vx + vy * vy);
	}

	public double getDistanceTo(RoundShape o) {
		return Math.sqrt((this.x - o.x) * (this.x - o.x) + (this.y - o.y) * (this.y - o.y)) - (this.rad - o.rad) / 2;
	}

	public double getAngleWith(RoundShape o) {
		return Math.atan2(o.y - this.y, o.x - this.x);
	}
}
