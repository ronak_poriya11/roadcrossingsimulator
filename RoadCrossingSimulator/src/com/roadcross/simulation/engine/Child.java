package com.roadcross.simulation.engine;


import com.roadcross.simulation.neuralNetwork.DNA;
import com.roadcross.simulation.neuralNetwork.NeuralNet;
import com.roadcross.simulation.neuralNetwork.Stage;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;

public class Child {

    // Movement constants:
    public static final double padestrianCollisionThreshold = 4;
    // view constants:
    public static final double maximumSightDistance = 400;
    public static final double maximumSpeed = 0.2;
    // child can view vehicle +/- 120 angle when it tries to cross road
    public static final double fieldOfView = Math.PI*2/3 ;

    // neural net constants:
    public static final int NO_OF_DIVISION = 8;//120 is divided into 8 parts
    public static final int FIRSTSTAGESIZE = NO_OF_DIVISION * 2  * 2;//8 parts can contain padestrian on other hand / vehicle
    public static final int stageSizes[] = new int[]{FIRSTSTAGESIZE,15,8,2};

    // scoring constants:
    public static final double scoreAddOnPoints = 20;
    public static final int healthAddOnPoints = 10;
    public static final double healthdecrement = .02; // decremented each loop


    public RoundShape childHead;
    public DNA dna;
    public NeuralNet neuralNet;
    public double childAge = 1;
    //public double angle;
    public double score;
    public double health;
    public boolean isDead,hasReached;
    public double deathAlpha;
    public float hue;



    public Child(DNA dna, ExpressRoad expressRoad) {
        double x = Math.random() * (expressRoad.width - 2 * padestrianCollisionThreshold - 2 * RoundShape.globalRoundShapeRadius) + padestrianCollisionThreshold
                + RoundShape.globalRoundShapeRadius;
        double y = (expressRoad.height - 2 * padestrianCollisionThreshold - 2 * RoundShape.globalRoundShapeRadius);
        if (dna == null) {
            int dnalength = NeuralNet.calcNumberOfCoeffs(stageSizes, false) + 1;
            this.dna = new DNA(true, dnalength);
        } else {
            this.dna = dna;
        }

        childHead = new RoundShape(x, y, RoundShape.globalRoundShapeRadius);
        neuralNet = new NeuralNet(stageSizes);
        reloadFromDNA();
        score = 0;
        deathAlpha = 200;
        isDead = false;
        health = healthAddOnPoints * 3 / 2;
    }

    /**
     * reloads the network and the color from DNA
     */
    public void reloadFromDNA() {
        neuralNet.loadCoeffs(this.dna.data);
        this.hue = (float) this.dna.data[this.dna.data.length - 1] / 256f;
    }

    public boolean update(ExpressRoad expressRoad) {
        if (isDead || hasReached) {
            deathAlpha -= .6;
            return true;
        }

        childAge += 0.1;
        RoundShape head = childHead;
        double moveY =  calculateSpeed(expressRoad);
        head.vx = 0;
        head.vy=-maximumSpeed-moveY;



        // collision with padestrian:

        if (head.y - head.rad < padestrianCollisionThreshold) {
            score *= 5;
            hasReached = true;
            isDead = true;
        }

        if (head.y + head.rad > expressRoad.height - padestrianCollisionThreshold) {
            head.vy = -3;
            if(childAge>30){
                score /= 4;
                isDead = true;
            }
        }

        //RoundShape previous = head;
        head.updatePosition();

        boolean isVehicleCollided = false;
        for (RoundShape vehicle : expressRoad.getVehicleList()) {
            if (head.isColliding(vehicle, 0)) {
                health=0;
                isVehicleCollided = true;
                hue=100;
                break;
            }
        }

        if (!isVehicleCollided) {
            score +=  scoreAddOnPoints;
            health += healthAddOnPoints;
        }
        if (health > 3 * healthAddOnPoints) // saturate
            health = 3 * healthAddOnPoints;
        health -= healthdecrement;
        if (health <= 0) {
            isDead = true;
            score /= 4;
        }
        if(childAge>500){
            isDead = true;
            score/=2;
        }
        return !isDead;
    }


    public double getFitnessInfo() {
        return score + health;
    }


    public double calculateSpeed(ExpressRoad expressRoad) {
        // init input vector:
        VisualInput input[] = new VisualInput[NO_OF_DIVISION * 2];
        for (int i = 0; i < NO_OF_DIVISION * 2; i++)
            input[i] = new VisualInput();
        // vehicles:
        input = updateVisualInput(input, expressRoad.getVehicleList(), 0);
        //padestrian
        int step = (int) (maximumSightDistance * Math.sin(fieldOfView / (NO_OF_DIVISION * 1d))) / 20;
        LinkedList<RoundShape> padestrianObjs = new LinkedList<RoundShape>();
        for (int x = 0; x < expressRoad.width; x += step) {
            padestrianObjs.add(new RoundShape(x, 0, 1));
        }

        input = updateVisualInput(input, padestrianObjs, 1);
        double stageA[] = new double[FIRSTSTAGESIZE];

        for (int i = 0; i < NO_OF_DIVISION; i++) {
            stageA[input[i].type * NO_OF_DIVISION * 2 + i] = Stage.signalMultiplier * (maximumSightDistance - input[i].distance) / maximumSightDistance;
            stageA[input[i + NO_OF_DIVISION].type * NO_OF_DIVISION * 2 + NO_OF_DIVISION * 2 - 1 - i] = Stage.signalMultiplier
                    * (maximumSightDistance - input[i + NO_OF_DIVISION].distance) / maximumSightDistance;
        }

        double output[] = neuralNet.calc(stageA);
        double delta = output[1] - output[0];
        double speed =   10*maximumSpeed / Stage.signalMultiplier * delta;
        return speed;
    }


    private VisualInput[] updateVisualInput(VisualInput input[], List<RoundShape> objects, int type) {
        RoundShape head = childHead;
        for (RoundShape n : objects) {
            if (head == n)
                continue;

            double a = signedDoubleModulo(head.getAngleWith(n) + (Math.PI / 2), Math.PI * 2);

            double d = head.getDistanceTo(n);
            if (a >= 0 && a < fieldOfView) {
                if (d < input[(int) (a * NO_OF_DIVISION / fieldOfView)].distance) {
                    input[(int) (a * NO_OF_DIVISION / fieldOfView)].distance = d;
                    input[(int) (a * NO_OF_DIVISION / fieldOfView)].type = type;
                }
            } else if (a <= 0 && -a < fieldOfView) {
                if (d < input[(int) (-a * NO_OF_DIVISION / fieldOfView) + NO_OF_DIVISION].distance) {
                    input[(int) (-a * NO_OF_DIVISION / fieldOfView) + NO_OF_DIVISION].distance = d;
                    input[(int) (-a * NO_OF_DIVISION / fieldOfView) + NO_OF_DIVISION].type = type;
                }
            }
        }
        return input;
    }

    public static double doubleModulo(double a, double b){
        int k = (int)(a/b);
        if (a < 0) k--;
        return a-b*k;
    }
    public static double signedDoubleModulo(double a, double b){
        double c = doubleModulo(a, b);
        if (c >= b/2) c-=b;
        return c;
    }

    // Vehicle = 0;
    // Padestrian = 1;
    public class VisualInput {
        public double distance = maximumSightDistance;
        public int type = 0;
    }


    public void draw(Graphics g) {

        int alpha = (int) deathAlpha;

        Color c = new Color(Color.HSBtoRGB(hue, 1, 1));
        g.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), alpha));

        g.fillOval((int) (childHead.x - childHead.rad), (int) (childHead.y - childHead.rad), (int) (2 * childHead.rad + 1), (int) (2 * childHead.rad + 1));

        if(!isDead){
            RoundShape p = childHead; // get head
            double dist = p.rad / 2.3;
            double size = p.rad / 3.5;
            g.setColor(new Color(255, 255, 255, alpha));
            g.fillOval((int) (p.x + p.vy * dist / p.getSpeedInfo() - size), (int) (p.y - p.vx * dist / p.getSpeedInfo() - size),
                    (int) (size * 2 + 1), (int) (size * 2 + 1));
            g.fillOval((int) (p.x - p.vy * dist / p.getSpeedInfo() - size), (int) (p.y + p.vx * dist / p.getSpeedInfo() - size),
                    (int) (size * 2 + 1), (int) (size * 2 + 1));
            size = p.rad / 6;
            g.setColor(new Color(0, 0, 0, alpha));
            g.fillOval((int) (p.x + p.vy * dist / p.getSpeedInfo() - size), (int) (p.y - p.vx * dist / p.getSpeedInfo() - size),
                    (int) (size * 2 + 1), (int) (size * 2 + 1));
            g.fillOval((int) (p.x - p.vy * dist / p.getSpeedInfo() - size), (int) (p.y + p.vx * dist / p.getSpeedInfo() - size),
                    (int) (size * 2 + 1), (int) (size * 2 + 1));
        }


    }
}
