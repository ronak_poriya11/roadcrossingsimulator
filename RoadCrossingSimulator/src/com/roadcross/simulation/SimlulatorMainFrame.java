package com.roadcross.simulation;

import com.roadcross.simulation.engine.RoadCrossLooop;

import javax.swing.*;

public class SimlulatorMainFrame extends JFrame {

    public static void main(String[] args) {
        new SimlulatorMainFrame();
    }

    public SimlulatorMainFrame() {
        add(new RoadCrossLooop());
        setSize(1080, 700);
        setExtendedState(MAXIMIZED_BOTH);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }

}

