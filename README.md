RoadCrossingSimultor (Dev-InProgress):
   A Java application,Which guides children to cross roads and reach up-to pedestrian on next end.On running traffic condition,Child learns to stop walking while vehicle is nearby and start walking again when road is clear.For this case it uses neural network with 2 output nodes,That decides whether to slowdown or continue to walk further.To update weights in neural network,this application uses genetic algorithm.

Neural network architecture:

  Child looks for  120 +/- (Total angle : 4/3 * PI or 240) from current position to get idea about is it closer to pedestrian or any vehicle & that 240 angle divided into 16 division.



Input neurons :  32 neurons (16*2 ,Type 0 : vehicle ,Type 1 : Pedestrian) 
Hidden Layer 1:  15 neurons
Hidden Layer 2:   8 neurons
Output Layer  :   2 neurons (Slow down or continue to walk)

References :
[Nature of code series by Daniel shiffman](http://natureofcode.com/book/chapter-9-the-evolution-of-code/)